#!/usr/bin/env bash

echo "Creating Docker Image"
docker build -t 'flask-sample' - < Dockerfile
echo "Retrieving Installed Docker Images"
docker images
