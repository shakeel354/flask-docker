FROM chug/ubuntu14.04x32
MAINTAINER shakeel "shakeel3it@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN mkdir /home/app
RUN chmod -R 777 /home/app
WORKDIR /home/app
RUN echo "from flask import Flask\napp = Flask(__name__)\n@app.route('/')\ndef hello_world():\n\treturn 'Hello, World!'\n\nif __name__ == '__main__':\n\tapp.run(debug=True,host='0.0.0.0')" >> app.py
# COPY . /app
RUN echo "Flask==0.11.1\nJinja2==2.8\nMarkupSafe==0.23\nWerkzeug==0.11.11\nargparse==1.2.1\nclick==6.6\nitsdangerous==0.24\nwsgiref==0.1.2" >> requirements.txt
# WORKDIR /sample
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]