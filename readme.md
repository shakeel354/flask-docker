# flask app in docker


## if OS is  64 bit change first line in Dockerfile to  

``` FROM chug/ubuntu14.04x64 ```


## if OS is  32 bit change first line in Dockerfile to  

``` FROM chug/ubuntu14.04x32 ```


## To setup run

``` sh setup.sh ```

## After sucessfull setup RUN below command to run flask app in docker

``` docker run -d -p 5000:5000 flask-sample ```

## Access   127.0.0.1:5000